<?php
namespace interfaces;

interface DBInterface {
    function query(string $var);
}