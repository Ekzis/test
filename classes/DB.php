<?php 
class DB {
    private $host = "localhost";
    private $db_name = "test";
    private $username = "root";
    private $password = "";
    protected $conn;
    
    public function __construct() {
        $this->conn = null;

        try {
            $this->conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);
        } catch(Exception $exception) {
            echo "Ошибка соединения: " . $exception->getMessage();
        }
    }
   
}


