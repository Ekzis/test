<?php 
require_once './interfaces/DBInterface.php';
require_once './classes/DB.php';

use interfaces\DBInterface;
class MySql extends DB implements DBInterface {
    public function query(string $sql)
    {
        $query = $this->conn->query($sql);
        return (object)['rows'=> $this->queryToArray($query), 'num_rows' => $query->num_rows];
    }
    private function queryToArray(mysqli_result $sql)
    {
        return $sql->fetch_all(MYSQLI_ASSOC);
    }
}