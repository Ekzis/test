<?php


Class Date {
    private $date;
    private $format = 'Y-m-d';

    public function __construct(int $year, int $month, int $day)
    {
        if ($month > 12) {
            throw new Exception('Месяц указан неверно: ' . $month);
        }
      
        $this->validateParams(['year' => $year, 'mouth' => $month, 'day' => $day]);

        $this->date = DateTime::createFromFormat($this->format, implode('-', [$year, $month, $day]));
        /* Если указываем день больше, чем есть в месяце, то он начинает прибавлять его месяц и ведет отсчет от данной даты, нам такое поведение не нужно, так как мы указали определенный месяц */
        if ($month < $this->date->format('m') || $day > 31) {
            throw new Exception('День указан неверно: ' . $day);
        }
       
    }

    public function validateParams(array $params) {
        foreach ($params as $key => $param) {
            $param <=0 ? throw new Exception('Параметр '. $key . ' не должен быть меньше или равен нулю') : true;
        }
    }
    public function getDate() {
        return $this->date;
    }
    public function getDateString(string $pattern) {
        return $this->date->format($pattern);
    }
}