<?php 
namespace classes;

use Date;
use Exception;

class Tuesdays {
    private $firstDay;
    private $lastDay;
    private $diff;

    function __construct(Date $dateFrom,  Date $dateTo)
    {
        if ($dateFrom->getDate() > $dateTo->getDate()) {
            throw new Exception('$dateFrom Должен быть мньше или равен $dateTo!');
        } else {
            $this->firstDay = $this->getDayOfWeek($dateFrom);
            $this->lastDay = $this->getDayOfWeek($dateTo);

            $this->diff = $this->getDiff($dateFrom, $dateTo);
            $this->trim();
        }
    }

    public function getDiff(Date $dateFrom, Date $dateTo) {
        return (int)$dateFrom->getDate()->diff( $dateTo->getDate() )->format('%a') + 1;
    }

    public function getDayOfWeek(Date $date) {
        return date("w", strtotime($date->getDate()->format('Y-m-d')));
    }

    private function trim() {
        // Обрезаем слева до вторника
         $this->diff -= $this->firstDay > 2 ? (7 - $this->firstDay + 2) : (2 - $this->firstDay);

         // Обрезаем справа до вторника
         $this->diff -= $this->lastDay >= 2 ? ($this->lastDay - 2) : (7 - $this->lastDay - 2);
    }

    public function getTuesdays() {
        return floor($this->diff / 7) + 1;
    }
}