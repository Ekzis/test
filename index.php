<?php

require_once('./classes/MySql.php');
require_once('./classes/Date.php');
require_once('./classes/Tuesdays.php');

use classes\Tuesdays as Tuesdays;


$db = new MySql();

/* Задание #1 */

$quest1 = $db->query('SELECT * FROM `users` AS u where posts_qty > (SELECT posts_qty FROM `users` WHERE id = u.invited_by_user_id)');
echo "Задание 1: <br>";
print_r($quest1->rows);
echo '<br><br>';

$quest2 = $db->query('SELECT u.* FROM `users` u LEFT JOIN `users` u2 ON u.group_id = u2.group_id AND (u.posts_qty < u2.posts_qty /*  */) WHERE u2.posts_qty is NULL');
// Таким образом мы получим всех пользователей с максимальным кол-во постов в группе, если в группе одинаковое кол-во постов у двух людей, то они будут отображены вместе, если такое поведение не нужно, то можно добавить 'or (u.posts_qty = u2.posts_qty and u.id < u2.id)' в /*  */
echo "Задание 2: <br>";
print_r($quest2->rows);
echo '<br><br>';

$quest3 = $db->query('SELECT group_id, count(*) as cnt from users group by group_id HAVING cnt > 10000');
echo "Задание 3: <br>";
print_r($quest3->rows);
echo '<br><br>';

$quest4 = $db->query('SELECT * FROM `users` AS u where group_id != (SELECT group_id FROM `users` WHERE id = u.invited_by_user_id)');
echo "Задание 4: <br>";
print_r($quest4->rows);
echo '<br><br>';

$quest5 = $db->query('SELECT group_id, sum(posts_qty) as cnt from users group by group_id having cnt >= ALL(select sum(posts_qty) from users group by group_id)');
// Таким образом мы получим все группы с максимальным кол-вом постов у пользователей, если таких групп несколько, то он выведет их всех. Если нужна только одна группа из всех, то можно использовать такой запрос - SELECT group_id, sum(posts_qty) as cnt FROM `users` GROUP BY group_id ORDER BY cnt DESC LIMIT 1
echo "Задание 5: <br>";
print_r($quest5->rows);
echo '<br><br>';


echo '
Задание #2 <br>
    Я бы попытался сделать это через систему очередей, создаем пустую, уже измененную таблицу,
также создаем тригеры, чтобы отслеживать, какие операциипроходили, пока мы выкачивали данные. Далее порционоо
начинаем перегонять данные из одной таблицы, в другую, после производим rename таблицы. Как я знаю, такая штука
уже реализована в открытом доступе у facebook и еще у кого-то
<br>
    Также, можно сделать master-slave репликацию и сделать данный alert там после чего уже поменять их.
<br>
<br>
';



echo 'Задание #3 <br>';

$datetimeFrom = new Date(2022, 9, 1);
$datetimeTo = new Date(2022, 9, 30);

$tuesdays = new Tuesdays($datetimeFrom, $datetimeTo);

echo 'Начальная дата: ' . $datetimeFrom->getDateString('Y-m-d') . '<br>';
echo 'Конечная дата: ' . $datetimeTo->getDateString('Y-m-d') . '<br>';
echo 'Количество вторников: ' . $tuesdays->getTuesdays();